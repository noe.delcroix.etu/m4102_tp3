package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());

	private CommandeDao dao;
	private IngredientDao ingredientsDao;
	private PizzaDao pizzasDao;


	private List<Ingredient> ingredientsOfPizzas=new ArrayList<Ingredient>();
	private List<Pizza> pizzasOfCommande = new ArrayList<Pizza>();

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	
	
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		dao.createTableAndIngredientAssociation();	

		pizzasDao = BDDFactory.buildDao(PizzaDao.class);
		pizzasDao.createTableAndIngredientAssociation();	
		
		ingredientsDao = BDDFactory.buildDao(IngredientDao.class);
		ingredientsDao.createTable();	
		
		ingredientsOfPizzas.add(new Ingredient("cornichon"));
		ingredientsOfPizzas.add(new Ingredient("nutella"));
		
		for (Ingredient ingredient : ingredientsOfPizzas) {
			ingredientsDao.insert(ingredient);
		}
				
		Pizza pizza1 = new Pizza();
		pizza1.setName("Pizza1");
		pizza1.setPricesmall(15.2);
		pizza1.setPricelarge(9999.99);
		pizza1.setIngredients(ingredientsOfPizzas);
		pizzasOfCommande.add(pizza1);
		
		Pizza pizza2 = new Pizza();
		pizza2.setName("Pizza2");
		pizza2.setPricesmall(10.2);
		pizza2.setPricelarge(666.666);
		pizza2.setIngredients(ingredientsOfPizzas);
		pizzasOfCommande.add(pizza2);
		
		for (Pizza pizza : pizzasOfCommande) {
			pizzasDao.insert(pizza);
		}
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndIngredientAssociation();
		pizzasDao.dropTableAndIngredientAssociation();
		ingredientsDao.dropTable();
	}

	
	@Test
	
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe import fr.ulille.iut.pizzaland.beans.IngredientResponse permet de traiter la réponse HTTP reçue.
		Response response = target("/commandes").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commandes.size());
	}

	
	@Test
	public void testGetExistingCommande() {

		Commande commande = new Commande();
		commande.setName("Noé Delcroix");
		commande.setPizzas(pizzasOfCommande);
		dao.insert(commande);
		

		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
	}

	
	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes/").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	

	@Test
	public void testCreateCommande() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		commandeCreateDto.setName("Noé DELCROIX");
		commandeCreateDto.setPizzas(pizzasOfCommande);

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
		assertEquals(returnedEntity.getPizzas(), commandeCreateDto.getPizzas());

	}



	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande();
		commande.setName("Noé DELCROIX");
		commande.setPizzas(pizzasOfCommande);
		dao.insert(commande);

		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}


	@Test
	public void testCreatePizzaWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}


	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("A supprimer");
		commande.setPizzas(pizzasOfCommande);
		dao.insert(commande);

		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Commande result = dao.findById(commande.getId());
		assertEquals(result, null);
	}


	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}


	@Test
	public void testGetCommandeName() {
		Commande commande = new Commande();
		commande.setName("Noé DELCROIX");
		commande.setPizzas(pizzasOfCommande);
		dao.insert(commande);


		Response response = target("/commandes").path(commande.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Noé DELCROIX", response.readEntity(String.class));
	}


	@Test
	public void testGetCommandePizzas() {
		Commande commande = new Commande();
		commande.setName("Noé DELCROIX");
		commande.setPizzas(pizzasOfCommande);
		dao.insert(commande);

		Response response = target("commandes").path(commande.getId().toString()).path("pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		String l =response.readEntity(String.class);

		String ingredientString="[{";
		for(int i = 0; i<ingredientsOfPizzas.size();i++) {
			ingredientString+="\"id\":\""+ingredientsOfPizzas.get(i).getId().toString()+"\",\"name\":\""+ingredientsOfPizzas.get(i).getName()+(i==ingredientsOfPizzas.size()-1 ? "\"}]" : "\"},{" );
		}
		
		String pizzasString="[{";
		for (int p=0;p<pizzasOfCommande.size();p++) {
			Pizza pizza=pizzasOfCommande.get(p);
			pizzasString+="\"id\":\""+pizza.getId()+"\",\"ingredients\":"+ingredientString+",\"name\":\""+pizza.getName()+"\",\"pricelarge\":"+pizza.getPricelarge()+",\"pricesmall\":"+pizza.getPricesmall()+(p==pizzasOfCommande.size()-1 ? "}]" : "},{" );
		}
		
		assertEquals(l,pizzasString);
	}



	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}


	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "Noé DELCROIX");
		form.param("pizzas", "Pizza1,Pizza2");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("/commandes").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Commande result = dao.findById(UUID.fromString(id));
		
		System.out.println("\n\n\n"+result+"\n\n\n");

		assertNotNull(result);
	}


	@Test
	public void testCreateWithFormButIngredientDoNotExist() {
		Form form = new Form();
		form.param("name", "Commande invalide");
		form.param("ingredients", "pizza pas existante,  ,yo");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("commandes").request().post(formEntity);

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
}
