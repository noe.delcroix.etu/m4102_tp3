package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());

	private PizzaDao dao;
	private IngredientDao ingredientsDao;

	private List<Ingredient> ingredientsOfPizzas=new ArrayList<Ingredient>();

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndIngredientAssociation();	

		ingredientsDao = BDDFactory.buildDao(IngredientDao.class);
		ingredientsDao.createTable();		
		ingredientsOfPizzas.add(new Ingredient("cornichon"));
		ingredientsOfPizzas.add(new Ingredient("nutella"));

		for (Ingredient ingredient : ingredientsOfPizzas) {
			ingredientsDao.insert(ingredient);
		}


	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndIngredientAssociation();
		ingredientsDao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe import fr.ulille.iut.pizzaland.beans.IngredientResponse permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDao> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDao>>() {
		});

		assertEquals(0, pizzas.size());
	}


	@Test
	public void testGetExistingPizza() {

		Pizza pizza = new Pizza();
		pizza.setName("la pizza");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);
		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Pizza");
		pizzaCreateDto.setPricelarge(42.0);
		pizzaCreateDto.setPricesmall(42.0);
		pizzaCreateDto.setIngredients(ingredientsOfPizzas);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
		assertEquals(returnedEntity.getPricelarge(), pizzaCreateDto.getPricelarge() ,0.01);
		assertEquals(returnedEntity.getPricesmall(), pizzaCreateDto.getPricesmall() ,0.01);
		assertEquals(returnedEntity.getIngredients(), pizzaCreateDto.getIngredients());

	}



	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);
		dao.insert(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}


	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}


	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza a supprimer");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);
		dao.insert(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}


	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}


	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza avec un nom sympa");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);

		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Pizza avec un nom sympa", response.readEntity(String.class));
	}


	@Test
	public void testGetPizzaIngredients() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza là");
		pizza.setPricelarge(10);
		pizza.setPricesmall(5);
		pizza.setIngredients(ingredientsOfPizzas);
		dao.insert(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("ingredients").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		String l =response.readEntity(String.class);
		String ingredientString="[{";
		for(int i = 0; i<ingredientsOfPizzas.size();i++) {
			ingredientString+="\"id\":\""+ingredientsOfPizzas.get(i).getId().toString()+"\",\"name\":\""+ingredientsOfPizzas.get(i).getName()+(i==ingredientsOfPizzas.size()-1 ? "\"}]" : "\"},{" );
		}
		assertEquals(l,ingredientString);
	}



	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}


	@Test
	public void testGetPizzaPriceSmall() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza avec price small");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);

		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("pricesmall").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals(14.3, response.readEntity(double.class),0.01);
	}

	@Test
	public void testGetNotExistingPizzaPriceSmall() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).path("pricesmall").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaPriceLarge() {
		Pizza pizza = new Pizza();
		pizza.setName("Pizza avec price large");
		pizza.setPricelarge(15.2);
		pizza.setPricesmall(14.3);
		pizza.setIngredients(ingredientsOfPizzas);

		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("pricelarge").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals(15.2, response.readEntity(double.class),0.01);
	}

	@Test
	public void testGetNotExistingPizzaPriceLarge() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).path("pricesmlarge").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}


	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "je suis une pizza");
		form.param("pricesmall", "15.2");
		form.param("pricelarge", "14.2");
		form.param("ingredients", "cornichon,nutella");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("/pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = dao.findById(UUID.fromString(id));

		assertNotNull(result);
	}


	@Test
	public void testCreateWithFormButIngredientDoNotExist() {
		Form form = new Form();
		form.param("name", "chorizopizza");
		form.param("pricelarge", "10.0");
		form.param("pricesmall", "5.0");
		form.param("ingredients", "Pizza,Fromage");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
}
