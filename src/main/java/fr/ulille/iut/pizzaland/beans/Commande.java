package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Commande {
	private UUID id = UUID.randomUUID();
	private String name;

	private List<Pizza> pizzas;

	public Commande() {
	}

	public Commande(String name) {
		this.name = name;
	}

	public Commande(UUID id, String name) {
		this.id = id;
		this.name = name;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public static CommandeDto toDto(Commande i) {
		CommandeDto dto = new CommandeDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setPizzas(i.getPizzas());

        return dto;
    }

	public static Commande fromDto(CommandeDto dto) {
		Commande commande = new Commande();
		commande.setId(dto.getId());
		commande.setName(dto.getName());
		commande.setPizzas(dto.getPizzas());

		return commande;
	}

	public static CommandeCreateDto toCreateDto(Commande commande) {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setName(commande.getName());
		dto.setPizzas(commande.getPizzas());

		return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
		Commande commande = new Commande();
		commande.setName(dto.getName());
		commande.setPizzas(dto.getPizzas());

		return commande;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, pizzas);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		return Objects.equals(id, other.id) && Objects.equals(name, other.name) && Objects.equals(pizzas, other.pizzas);
	}

	@Override
	public String toString() {
		return "Commande [id=" + id + ", name=" + name + ", pizzas=" + pizzas + "]";
	}
}
