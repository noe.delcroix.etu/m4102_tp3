package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
    private String name;
    private double pricesmall;
    private double pricelarge;
	
    private List<Ingredient> ingredients=new ArrayList<Ingredient>();


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPricesmall() {
		return pricesmall;
	}

	public void setPricesmall(double pricesmall) {
		this.pricesmall = pricesmall;
	}

	public double getPricelarge() {
		return pricelarge;
	}

	public void setPricelarge(double pricelarge) {
		this.pricelarge = pricelarge;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setPricesmall(i.getPricesmall());
        dto.setPricelarge(i.getPricelarge());
        dto.setIngredients(i.getIngredients());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto dto) {
    	Pizza pizza = new Pizza();
    	pizza.setId(dto.getId());
    	pizza.setName(dto.getName());
    	pizza.setPricesmall(dto.getPricesmall());
    	pizza.setPricelarge(dto.getPricelarge());
    	pizza.setIngredients(dto.getIngredients());
    	
        return pizza;
    }
	
    public static Pizza fromIngredientCreateDto(PizzaCreateDto dto) {
    	Pizza pizza = new Pizza();
    	pizza.setName(dto.getName());
    	pizza.setPricelarge(dto.getPricelarge());
    	pizza.setPricesmall(dto.getPricesmall());
    	pizza.setIngredients(dto.getIngredients());

        return pizza;
      }

    public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
        dto.setPricelarge(pizza.getPricelarge());
        dto.setPricesmall(pizza.getPricesmall());
        dto.setIngredients(pizza.getIngredients());
            
        return dto;
      }
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(pricelarge);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(pricesmall);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(pricelarge) != Double.doubleToLongBits(other.pricelarge))
			return false;
		if (Double.doubleToLongBits(pricesmall) != Double.doubleToLongBits(other.pricesmall))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", pricesmall=" + pricesmall + ", pricelarge=" + pricelarge
				+ ", ingredients=" + ingredients + "]";
	}
    
	
}
