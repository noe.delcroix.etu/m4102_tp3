package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
    private double pricesmall;
    private double pricelarge;
    
    private List<Ingredient> ingredients=new ArrayList<Ingredient>();
	
	public PizzaCreateDto() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPricesmall() {
		return pricesmall;
	}

	public void setPricesmall(double pricesmall) {
		this.pricesmall = pricesmall;
	}

	public double getPricelarge() {
		return pricelarge;
	}

	public void setPricelarge(double pricelarge) {
		this.pricelarge = pricelarge;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	
}
