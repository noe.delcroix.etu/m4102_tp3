package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private UUID id = UUID.randomUUID();
    private String name;
    private double pricesmall;
    private double pricelarge;
	
    private List<Ingredient> ingredients;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPricesmall() {
		return pricesmall;
	}

	public void setPricesmall(double pricesmall) {
		this.pricesmall = pricesmall;
	}

	public double getPricelarge() {
		return pricelarge;
	}

	public void setPricelarge(double pricelarge) {
		this.pricelarge = pricelarge;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
}	
