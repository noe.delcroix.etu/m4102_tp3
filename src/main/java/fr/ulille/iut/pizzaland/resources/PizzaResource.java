package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());

	private PizzaDao pizzas;
	private IngredientDao ingredients;

	@Context
	public UriInfo uriInfo;

	public PizzaResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();

		ingredients = BDDFactory.buildDao(IngredientDao.class);
		ingredients.createTable();

	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaResource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());

		for (PizzaDto pizzaDto : l) {
			List<UUID> ingredientsID=pizzas.findByIdIngredients(pizzaDto.getId());
			List<Ingredient> pizzaIngredients=new ArrayList<Ingredient>();
			for (UUID idIngredient : ingredientsID) {
				pizzaIngredients.add(ingredients.findById(idIngredient));
			}
			pizzaDto.setIngredients(pizzaIngredients);
		}

		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		
		
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);

			List<UUID> ingredientsID=pizzas.findByIdIngredients(id);
			List<Ingredient> pizzaIngredients=new ArrayList<Ingredient>();
			for (UUID idIngredient : ingredientsID) {
				pizzaIngredients.add(ingredients.findById(idIngredient));
			}
			pizza.setIngredients(pizzaIngredients);

			LOGGER.info(pizza.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = Pizza.fromIngredientCreateDto(pizzaCreateDto);
			pizzas.insert(pizza);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}
	
	@GET
	@Path("{id}/ingredients")
	public List<Ingredient> getPizzaIngredients(@PathParam("id") UUID id) {
		Pizza pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		List<UUID> truc = pizzas.findByIdIngredients(id);
		List<Ingredient> lIngredient = new ArrayList<Ingredient>();
		
		for(UUID uuid : truc) {
			lIngredient.add(ingredients.findById(uuid));
		}
		return lIngredient;
	}


	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") UUID id) {
		if ( pizzas.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") UUID id) {
		Pizza pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@GET
	@Path("{id}/pricesmall")
	public double getPizzaPriceSmall(@PathParam("id") UUID id) {
		Pizza pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getPricesmall();
	}

	@GET
	@Path("{id}/pricelarge")
	public double getPizzaPriceLarge(@PathParam("id") UUID id) {
		Pizza pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getPricelarge();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizza(@FormParam("name") String name,@FormParam("pricesmall") String pricesmall,@FormParam("pricelarge") String pricelarge,@FormParam("ingredients") String ingredientNames) {
		Pizza existing = pizzas.findByName(name);

		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = new Pizza();
			pizza.setName(name);
			pizza.setPricelarge(Double.parseDouble(pricelarge));
			pizza.setPricesmall(Double.parseDouble(pricesmall));
			
			List<String> ingredientNamesList=Arrays.asList(ingredientNames.split(","));
			List<Ingredient> ingredientsOfPizza=new ArrayList<Ingredient>();
			for (String ingredientName : ingredientNamesList) {
				Ingredient ingredient = ingredients.findByName(ingredientName);
				ingredientsOfPizza.add(ingredient);
			}
			pizza.setIngredients(ingredientsOfPizza);

			pizzas.insert(pizza);

			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

}
