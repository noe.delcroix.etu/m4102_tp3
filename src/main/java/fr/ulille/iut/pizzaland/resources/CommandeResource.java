package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/commandes")
public class CommandeResource {
	
	
	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());

	private IngredientDao ingredients;
	private CommandeDao commandes;
	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createAssociationTable();

		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createAssociationTable();
		
		ingredients = BDDFactory.buildDao(IngredientDao.class);
		ingredients.createTable();

	}

	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeResource:getAll");

		List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());

		for (CommandeDto commandeDto : l) {
			List<UUID> pizzasID=pizzas.findByIdIngredients(commandeDto.getId());
			List<Pizza> commandePizzas=new ArrayList<Pizza>();
			for (UUID idPizza: pizzasID) {
				commandePizzas.add(pizzas.findById(idPizza));
			}
			commandeDto.setPizzas(commandePizzas);
		}

		LOGGER.info(l.toString());
		return l;
	}
	
	
	
	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commande = commandes.findById(id);
			
			List<UUID> pizzasID=commandes.findByIdPizzas(id);
			List<Pizza> commandePizzas=new ArrayList<Pizza>();
			for (UUID idPizza : pizzasID) {
				Pizza pizza=pizzas.findById(idPizza);
				
				List<UUID> ingredientsID=pizzas.findByIdIngredients(idPizza);
				List<Ingredient> pizzaIngredients=new ArrayList<Ingredient>();
				for (UUID idIngredient : ingredientsID) {
					pizzaIngredients.add(ingredients.findById(idIngredient));
				}
				pizza.setIngredients(pizzaIngredients);
				
				commandePizzas.add(pizza);
			}
			
			commande.setPizzas(commandePizzas);

			LOGGER.info(commande.toString());

			return Commande.toDto(commande);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		
	}
	
	@POST
	public Response createCommande(CommandeCreateDto commandeCreateDto) {
		Commande existing = commandes.findByName(commandeCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande commande = Commande.fromCommandeCreateDto(commandeCreateDto);
			commandes.insert(commande);
			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path(commande.getId().toString()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if ( commandes.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	
	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commande.getName();
	}
	
	
	@GET
	@Path("{id}/pizzas")
	public List<Pizza> getCommandesPizzas(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		List<UUID> pizzasID=commandes.findByIdPizzas(id);
		List<Pizza> commandePizzas=new ArrayList<Pizza>();
		for (UUID idPizza : pizzasID) {
			Pizza pizza=pizzas.findById(idPizza);
			
			List<UUID> ingredientsID=pizzas.findByIdIngredients(idPizza);
			List<Ingredient> pizzaIngredients=new ArrayList<Ingredient>();
			for (UUID idIngredient : ingredientsID) {
				pizzaIngredients.add(ingredients.findById(idIngredient));
			}
			pizza.setIngredients(pizzaIngredients);
						
			commandePizzas.add(pizza);
		}
		
		return commandePizzas;
	}
	
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createCommande(@FormParam("name") String name,@FormParam("pizzas") String pizzaNames) {
		Commande existing = commandes.findByName(name);

		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
	
		try {
			Commande commande = new Commande();
			commande.setName(name);

			
			List<String> pizzaNamesList=Arrays.asList(pizzaNames.split(","));
			List<Pizza> pizzaOfCommande=new ArrayList<Pizza>();
			for (String pizzaName : pizzaNamesList) {
				Pizza pizza = pizzas.findByName(pizzaName);
				
				List<UUID> ingredientsID=pizzas.findByIdIngredients(pizza.getId());
				List<Ingredient> pizzaIngredients=new ArrayList<Ingredient>();
				for (UUID idIngredient : ingredientsID) {
					pizzaIngredients.add(ingredients.findById(idIngredient));
				}
				pizza.setIngredients(pizzaIngredients);
				
				pizzaOfCommande.add(pizza);
			}
			
			commande.setPizzas(pizzaOfCommande);
			
			commandes.insert(commande);

			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();
			
			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
}
