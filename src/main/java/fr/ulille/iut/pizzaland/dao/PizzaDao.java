package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
	@Transaction
    default void createTableAndIngredientAssociation() {
      createAssociationTable();
      createPizzaTable();
    }
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY,name VARCHAR UNIQUE NOT NULL,pricesmall REAL, pricelarge REAL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation (idpizza VARCHAR(128),"
    		+ "idingredient VARCHAR(128),"
    		+ "FOREIGN KEY(idpizza) REFERENCES pizzas(id) , "
    		+ "FOREIGN KEY(idingredient) REFERENCES ingredients(id) )")
    void createAssociationTable();
    
    
    @Transaction
    default void dropTableAndIngredientAssociation() {
    	dropAssociationTable();
    	dropPizzaTable();
    }
    
    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropPizzaTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS pizzaIngredientsAssociation")
    void dropAssociationTable();

    @Transaction
    default void insert(Pizza pizza) {
    	insertPizza(pizza);
    	for (Ingredient ingredient : pizza.getIngredients()) {
    		insertAssociation(pizza.getId(),ingredient.getId());
    		
    	}
    }
    
    @SqlUpdate("INSERT INTO pizzas (id, name, pricesmall, pricelarge) VALUES (:id, :name, :pricesmall, :pricelarge)")
    void insertPizza(@BindBean Pizza pizza);
    
    @SqlUpdate("INSERT INTO pizzaIngredientsAssociation (idpizza,idingredient) VALUES (:idpizza, :idingredient)")
    void insertAssociation(@Bind("idpizza") UUID idpizza,@Bind("idingredient") UUID idingredient);
    
    
    @Transaction
    default void remove(UUID id) {
    	removeAssociations(id);
    	removePizza(id);
    }
    
    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void removePizza(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM pizzaIngredientsAssociation WHERE idpizza = :idpizza")
    void removeAssociations(@Bind("idpizza") UUID idpizza);
    
  
   
    
    
    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);
    
    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
    
    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT idingredient FROM pizzaIngredientsAssociation WHERE idpizza=:id")
    List<UUID> findByIdIngredients(@Bind("id") UUID id);
}
