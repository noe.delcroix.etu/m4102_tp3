package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {
	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createCommandeTable();
	}

	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id VARCHAR(128) PRIMARY KEY,name VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaCommandeAssociation (idpizza VARCHAR(128),"
			+ "idcommande VARCHAR(128),"
			+ "FOREIGN KEY(idpizza) REFERENCES pizzas(id) , "
			+ "FOREIGN KEY(idcommande) REFERENCES commandes(id) )")
	void createAssociationTable();


	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropAssociationTable();
		dropCommandeTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS commandes")
	void dropCommandeTable();

	@SqlUpdate("DROP TABLE IF EXISTS pizzaCommandeAssociation")
	void dropAssociationTable();

	@Transaction
	default void insert(Commande commande) {
		insertCommande(commande);
		for (Pizza pizza : commande.getPizzas()) {
			insertAssociation(pizza.getId(),commande.getId());

		}
	}

	@SqlUpdate("INSERT INTO commandes (id, name) VALUES (:id, :name)")
	void insertCommande(@BindBean Commande commande);

	@SqlUpdate("INSERT INTO pizzaCommandeAssociation (idpizza,idcommande) VALUES (:idpizza, :idcommande)")
	void insertAssociation(@Bind("idpizza") UUID idpizza,@Bind("idcommande") UUID idcommande);


	@Transaction
	default void remove(UUID id) {
		removeAssociations(id);
		removeCommande(id);
	}

	@SqlUpdate("DELETE FROM commandes WHERE id = :id")
	void removeCommande(@Bind("id") UUID id);

	@SqlUpdate("DELETE FROM pizzaCommandeAssociation WHERE idcommande = :idcommande")
	void removeAssociations(@Bind("idcommande") UUID idcommande);


	@SqlQuery("SELECT * FROM commandes WHERE name = :name")
	@RegisterBeanMapper(Commande.class)
	Commande findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();

	@SqlQuery("SELECT * FROM commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(@Bind("id") UUID id);

	@SqlQuery("SELECT idpizza FROM pizzaCommandeAssociation WHERE idcommande=:id")
	List<UUID> findByIdPizzas(@Bind("id") UUID id);
}
