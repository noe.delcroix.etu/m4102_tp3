| URI | Opération | MIME | Requête | Réponse |
|-|-|-|-|-|
| /commandes | GET | <-application/json<br><-application/xml | | liste des commandes |
| /commandes/{id} | GET | <-application/json<br><-application/xml | | une commande ou 404 |
| /commandes/{id}/name | GET | <-text/plain | | le nom de personne qui a fait la commande ou 404 
| /commandes/{id}/pizzas | GET | <-text/plain | | les pizzas que la commande contient ou 404 |
| /commandes | POST | <-/->application/json | Commande | Nouvelle Commande <br>409 si la pizza existe déjà (même nom) |
| /commandes | POST | <-/->application/json<br>->application/x-www-form-urlencoded | Commande | Nouvelle Commande <br>409 si la pizza existe déjà (même nom) |
| /commandes/{id} | DELETE | | | |