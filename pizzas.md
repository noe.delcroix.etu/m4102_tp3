| URI | Opération | MIME | Requête | Réponse |
|-|-|-|-|-|
| /pizzas | GET | <-application/json<br><-application/xml |  | liste des pizzas |
| /pizzas/{id}  | GET | <-application/json<br><-application/xml |  | une pizza ou 404 |
| /pizzas/{id}/name | GET | <-text/plain |  | le nom de la pizza ou 404 |
| /pizzas/{id}/pricesmall | GET | <-text/plain |  | le prix de la pizza petite ou 404 |
| /pizzas/{id}/pricelarge | GET | <-text/plain |  | le prix de la pizza grande ou 404 |
| /pizzas/{id}/ingredients | GET | <-application/json<br><-application/xml  |  | la liste des ingredients de la pizza ou 404 |
| /pizzas | POST | <-/->application/json | Pizza | Nouvelle Pizza <br>409 si la pizza existe déjà (même nom) |
| /pizzas | POST | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza | Nouvelle Pizza <br>409 si la pizza existe déjà (même nom) |

| /pizzas/{id}  | DELETE |  |  |  |